*** Settings ***
Library           Selenium2Library
Resource          Sing_Keywords_Resource/sing-keywords-file.robot
Variables         Sing_Variables/sing-variables.py
Variables         Sing_Xpaths/sing-xpath.py

*** Test Cases ***
Test step 1: Open browser                           Open Browser for Run Test
Test step 2: SignIn Account                         Log In                                          Username
Test step 3: Click Task Menu                        Click Task Menu
Test step 4: Select Task OP Menu                    Click Task OP
Test step 5: Click btn Create New OP                Click Btn Create OP
Test step 6: Input Data Request Info                Input Text Request Info OP Page
Test step 7: Input Data Customer Contact            Input Customer Contact OP Page
Test step 8: Input Data Resource Bundling           Input Resource Bundling List OP Page
Test step 9: Input Data Requirement List            Input Requirement List OP Page
Test step 10: Input Data Attachment                 Input Attachment OP Page
Test step 11: Input Data Remark                     Input Remark OP Page
Test step 12: Click Save Draft                      Click Save Draft