*** Settings ***
Library           Selenium2Library
Resource          Sing_Keywords_Resource/sing-keywords-file.robot
Variables         Sing_Variables/sing-variables.py
Variables         Sing_Xpaths/sing-xpath.py

*** Test Cases ***
Test step 1: Open browser                           Open Browser for Run Test
Test step 2: SignIn Account                         Log In                                 Username
Test step 3: Click Task Menu                        Click Task Menu
Test step 4: Select Task OP Menu                    Click Task OP
Test step 5: Search Task                            Search Task OP BY Project Code


