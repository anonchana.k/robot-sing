# This Python file uses the following encoding: utf-8
import os, sys

# *************Login Page *****************
USERNAME = '//input[@id="username"]'
LOGIN_BUTTON = '//button[@class="btnaction btn-w-m btn-ais m-r-xxs"]'


# *************** Dashbroad ***************
TASK_MENU = '//span[text()="Task"]'


# *************** Task Menu ***************
TASK_LIST_OP = '//span[text()="Task Opportunity Pointer"]'
CREATE_OP_BTN = '//a[@id="btnCreate"]'


# *************** Create OP ***************
# *************** Request Info *********************************
IP_PROJ_NAME = '//textarea[@id="txtProjectName"]'
IP_PROJ_DETAIL = '//textarea[@id="txtProjectDetail"]'
IP_TEN_CAR = '//div[@class="dx-button-content"]/div[@class="dx-dropdowneditor-icon"][1]'
IP_TEN_DATE = '//td[@class="dx-calendar-cell"][@data-value="2019/02/17"]'
SL_DD_INDUSTRY = '//select[@id="ddlcbIndustry"]'
SL_DATA_INDUSTRY = '//select[@id="ddlcbIndustry"]/option[@value="001"]'
T_DATE = '//div[@class="dx-texteditor-container"]//input[@class="dx-texteditor-input"]'

# ************** Customer Contact ******************************
IP_COMPANY = '//input[@id="txtCompany"]'
IP_NAME = '//input[@id="txtName"]'
IP_POSITION = '//input[@id="txtPosition"]'
IP_EMAIL = '//input[@id="txtEmail"]'
IP_PHONE = '//input[@id="txtPhoneNo"]'
CLICK_UL_FILE = '//input[@id="btnUploadImage"]'
IP_FILE = '//input[@id="txtFileName"]'
ID_FILE = '//input[@id="txtFileName"]'
CLICK_ADD_FILE = '//div[@id="customerContackPannel"]//button[@id="btnAddBusinessCard"][@name="btnAddBusinessCard"][1]'


# ************** Resource Bundling List ******************************
C_BTN_SR = '//button[@id="btnBundlingName"]'
FN_PAEG = '//div[@class="col-sm-7 pull-left m-l-n-md"]/div[@class="fontpage"][text()="Select Resource Name"]'
SL_CHECKBOX_F = '//tr['
SS = ']/td/div/div[@class="icheckbox_square-green disabled"]'
SL_CHECKED_F = '//tr/td/div/div[@class="icheckbox_square-green checked"]'
SL_CHECKBOX_S = '//tr[2]/td/div/div[@class="icheckbox_square-green disabled"]'
SL_CHECKED_S = '//tr[2]/td/div/div[@class="icheckbox_square-green checked"]'
C_BTN_OK = '//div[@class="row modal-footer-custom"]//a[@id="btnOkLookup"]'

# ************** Requirement List ******************************
IP_REQ_DETAIL = '//textarea[@id="txtRequirementDetail"]'
C_DD_SIGNIFICANT = '//select[@id="SignificantLevel"]'
SL_DATA_SIGNIFICANT = '//select[@id="SignificantLevel"]/option[@value="001"]'
C_ADD_BTN = '//div[@class="col-sm-6"]/button[@id="btnSearch"]'
C_EDIT_ICON = '//tbody/tr/td/div[contains(text(),"Test_Robot_001")]/../../td[last()]/div/button[@id="btnEdit"]'
# PRIOR = '//input[@id="txtPriority1"]'


# **************** Attachment **********************
BR_FILE_BTN = '//input[@id="fileAM"]'
TEXT_BOX_FILE = '//div[@id="Attachment"]//input[@id="FileNameAM"]'
C_ADD_AT_BTN = '//div[@id="Attachment"]//button[@id="btnAddBusinessCard"]'


# ******************** Remark *********************
TEXT_RE = '//div//..//span/b[contains(text(),"Remark")]'
IP_TEXT_RE = '//app-form-remark//textarea[@name="txtremarkDescription"]'


# ********************** Button ***********************
C_SD_BTN = '//div[@class="form-group pull-right"]/button[@id="btnSaveDraftTop"]'
C_CLOSE_BTN = '//div[not(contains(@style,"display:none"))]//div[@id="myModalSuccess"]//a[@id="btnCloseSuccess"]'


# ********************* Search 1 ************************
IP_PROJ_CODE_SEARCH = '//form[@id="frmSearch"]//input[@id="txtProjectCodeTeamTask"]'
IP_PROJ_NAME_SEARCH = '//form[@id="frmSearch"]//input[@id="txtProjectNameTeamTask"]'
SL_REQ_DATE_SEARCH = '//form[@id="frmSearch"]//dx-date-box[@id="calRequestDate"]//div[@class="dx-dropdowneditor-icon"]'
SL_DATE_SEARCH = '//td[@class="dx-calendar-cell"][@data-value="2019/02/18"]'
SEARCH_BTN = '//form[@id="frmSearch"]//button[@id="btnSearch"]'
 


# P_LOADER   = '//*[contains(@class, \'modal-open\')]'
# REGIST     = '//li/span[contains(.,\'Registration\')]'    #'//*[@id=\'subList_1\']'
# C_LOCATION = '//li[@id=\'subListItem_1_0\']/div'
# C_DIST     = '//div[contains(text(),\'Distribution Channel\')]/following-sibling::div/select'   # '//*[@id=\'type\']'
# C_CHN      = '//div[contains(text(),\'Channel Sale Group\')]/following-sibling::div/select'
# RESET_C_CHN   = '//div/button[contains(text(), \'Reset\')]'
# NEXT_CHN   = '//div/button[contains(text(), \'Next\')]'
# LOGOUT     = '//li/span[contains(., \'ออกจากระบบ\')]'     #'//*[@id=\'subList_4\']'

# # *************  Path  ABOUT SAP  **************** 
# SAP_MONITORING  = '//ul[@class=\'dropdown-item\']/li/div[contains(., \'SAP Monitoring\')]'
# SELECT_INTERFACE_TYPE     = '//div[contains(text(),\'Interface Type\')]/following-sibling::div/select'
# SEARCH_SAP_MONITORING  = '//div/button[contains(., \'Search\')]'
# SEARCH_MENU = '//li/span[contains(.,\'Search\')]'
# LOCATION_MENU = '//li[@id="subListItem_2_0"]/div'