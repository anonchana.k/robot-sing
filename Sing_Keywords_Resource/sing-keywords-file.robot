*** Keywords ***
Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    0.3

Log In  

    [Arguments]                             ${txtLogIn}
    Wait Until Page Contains                ${txtLogIn}
    Input Text        ${USERNAME}           ${USER}
    Click Button      ${LOGIN_BUTTON}


Log Out
    Click Element                       ${LOGOUT}


Click Task Menu
    click Element                       ${TASK_MENU}
    
Click Task OP
    click Element                       ${TASK_LIST_OP}

Click Btn Create OP
    Wait Until Element Is Visible       ${CREATE_OP_BTN}    
    click Element                       ${CREATE_OP_BTN}   

Search Task OP BY Project Code
    Input Text                          ${IP_PROJ_CODE_SEARCH}                     ${T_PROJ_CODE}
    Wait Until Element Is Visible       ${IP_PROJ_CODE_SEARCH}
    Click Button                        ${SEARCH_BTN}

Search Task OP BY Project Name 
    Input Text                          ${IP_PROJ_NAME_SEARCH}                     ${TEXT}
    Wait Until Element Is Visible       ${IP_PROJ_NAME_SEARCH}
    Click Element                       ${SEARCH_BTN}

Search Task OP BY Request Date  
    Click Element                       ${SL_REQ_DATE_SEARCH}
    Wait Until Element Is Visible       ${SL_REQ_DATE_SEARCH}
    Click Element                       ${SL_DATE_SEARCH}
    Wait Until Element Is Visible       ${T_DATE}
    Click Element                       ${SEARCH_BTN}

# ******************** After Search on Task List********************
Click View Icon on Task List   


Click Edit Icon on Task List   


Click Print Icon on Task List


Click Cancel Icon on Task List


Click Save As New Project Icon on Task List





# ******************** Click Button on Another Page ************************
Click Edit Button


Click Print Button


Click Cancel Button


Click Save As New Project Button


Click Back Button


Click Unclaim Button





# *************** Input Data ********************************

Input Text Request Info OP Page
    Wait Until Element Is Visible       ${IP_PROJ_NAME}
    Input Text                          ${IP_PROJ_NAME}             ${T_PROJ_NAME}
    Input Text                          ${IP_PROJ_DETAIL}           ${T_PROJ_DETAIL}
    click Element                       ${IP_TEN_CAR}
    click Element                       ${IP_TEN_DATE}
    click Element                       ${SL_DD_INDUSTRY}   
    Wait Until Element Is Visible       ${SL_DATA_INDUSTRY}        
    click Element                       ${SL_DATA_INDUSTRY}

Input Customer Contact OP Page
    Input Text                          ${IP_COMPANY}             ${T_CUS_COMP}
    Input Text                          ${IP_NAME}                ${T_CUS_NAME}
    Input Text                          ${IP_POSITION}            ${T_CUS_POSI}
    Input Text                          ${IP_EMAIL}               ${T_CUS_EMAIL}
    Input Text                          ${IP_PHONE}               ${T_CUS_PHONE}   
    # click Element                       ${CLICK_UL_FILE}           
    Choose File                         ${CLICK_UL_FILE}          ${Device_Path}
    Wait Until Element Is Visible       ${ID_FILE}
    Click Button                        ${CLICK_ADD_FILE}


Input Resource Bundling List OP Page 
    Click Element                       ${C_BTN_SR}
    Wait Until Element Is Visible       ${FN_PAEG}
    :FOR        ${INDEX}      IN RANGE      ${LENGTH}
    \      Click Element          ${SL_CHECKBOX_F}${INDEX}+1${SS}           
    Wait Until Element Is Visible       ${SL_CHECKED_F}    
    Click Element                       ${C_BTN_OK}            

Input Requirement List OP Page
    # INPUT TEXT
    :FOR            ${INDEX}      IN RANGE       ${LENGTH_S}
    \   Exit For Loop If    ${INDEX}==${LENGTH_S}
    \   Input Text                          ${IP_REQ_DETAIL}               ${TEXT}
    \   Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    \   Click Element                       ${C_DD_SIGNIFICANT}
    \   Wait Until Element Is Visible       ${C_DD_SIGNIFICANT}
    \   Click Element                       ${SL_DATA_SIGNIFICANT}
    \   Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    \   Click Element                       ${C_ADD_BTN}
    # EDIT TEXT
    Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    Click Element                       ${C_EDIT_ICON}
    Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    Input Text                          ${IP_REQ_DETAIL}                   ${TEXT1}
    Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    Click Element                       ${C_DD_SIGNIFICANT}
    Wait Until Element Is Visible       ${C_DD_SIGNIFICANT}
    Click Element                       ${SL_DATA_SIGNIFICANT}
    Wait Until Element Is Visible       ${IP_REQ_DETAIL}
    Click Element                       ${C_ADD_BTN}

Input Attachment OP Page
    Choose File                         ${BR_FILE_BTN}                      ${Device_Path}
    Wait Until Element Is Visible       ${TEXT_BOX_FILE}
    Click Button                        ${C_ADD_AT_BTN}

Input Remark OP Page
    Wait Until Element Is Visible       ${TEXT_RE}
    Input Text                          ${IP_TEXT_RE}                       ${TEXT}

Click Save Draft
    Click Element                       ${C_SD_BTN}                                             
    Click Element                       ${C_CLOSE_BTN}



